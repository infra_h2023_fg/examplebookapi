from flask import *
import json

app = Flask("book api")

books = [
	{'id':0,
	'title':'Dawnthief',
	'author':'James Barclay',
	'series':'The chronicles of the raven',
	'year_published':2003,
	'rating':'veryGood'},
	{'id':1,
	'title':'Magician Apprentice',
	'author':'Raymond Feist',
	'series':'The Riftwar Saga',
	'year_published':1982,
	'rating':'veryGood'},
	{'id':2,
	'title':'Firedrake',
	'author':'Richard Knaak',
	'series':'Legends of the Dragonream',
	'year_published':2000,
	'rating':'veryGood'},
	{'id':3,
	'title':'Thelma la licorne',
	'author':'Aaron Blabey',
	'series':'N/A',
	'year_published':2017,
	'rating':'amazing'}
]

@app.route("/")
def hello():
	return '''<h1>Library</h1>
			<p>A simple API with books</p>'''

@app.route("/books/all")
def getAll():
	return json.dumps(books)

def getSpecificBooks(paramName, paramValue):
	results = []
	for book in books:
		if book[paramName] == paramValue:
			results.append(book)
	return results

@app.route("/books/one")
def getOne():
	results = []
	if 'id' in request.args:
		try:
			id = int(request.args['id'])
		except:
			return make_response(json.dumps({'msg':'The argument provided for the id field needs to be an integer'}), 400)
		results = getSpecificBooks('id', id)
	elif 'rating' in request.args:
		rating = request.args['rating']
		results = getSpecificBooks('rating', rating)
	else:
		m = 'One field needs to be specified to search for single books. Use either id or rating'
		#Choix possibles selon les besoins:
		#return make_response(m, 400)
		#return make_response(json.dumps(m), 400)
		return make_response(json.dumps({'msg':m}), 400)

	return json.dumps(results)

if __name__ == '__main__':
	app.run(debug = True, host="0.0.0.0", port=15555)
